const backend = require("./backend/backend");
const http = require("http");
const socketManager = require("./backend/socket");
const socketio = require('socket.io');

const server = http.createServer(backend);
const io = socketio(server, {
    cors: {
      origin: '*'
    }
  });

const socket = new socketManager(io);

  
  


const PORT = 3000 || process.env.PORT;
server.listen(PORT, () => console.log(`Server running on port ${PORT}`));


//Der nachfolgende abschnitt ist für die Desktopinstallation

// const electron = require('electron');

// const { BrowserWindow, app } = electron  

// const  path  = require("path");
// const  url  = require("url");

// app.on('ready', () => {
  
//   const win = new BrowserWindow({
//     width: 1400,
//     minWidth: 1400,
//     height: 800,
//     minHeight: 800,
//     icon:'build/icon.ico'
//   })
  

//     win.loadURL(url.format({
//       pathname: __dirname + '\\angular_build\\UnfallAnzeigenFrontend\\index.html',
//       protocol: 'file:',
//       slashes: true
//     }))

//     // win.webContents.openDevTools()

//     win.removeMenu();

//     win.once('ready-to-show', () => {
//       win.show()
//     })
  
// });