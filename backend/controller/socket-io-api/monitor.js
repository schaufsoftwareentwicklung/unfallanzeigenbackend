const ConfigurationService = require('../../services/configurationService');
const WorkingService = require('../../services/workingService');
const MonitorService = require('../../services/monitorService');


var socket;
var workingService;
var configurationService;
var monitorService;

class socketIoAPI {
    constructor() {
        workingService = new WorkingService().getInstance();
        configurationService = new ConfigurationService().getInstance();
        monitorService = new MonitorService().getInstance();
    }


    getDisplays() {
        try {
            monitorService.getDisplays()
                .then((displays) => {
                    socket.emit('sendDisplays', displays);
                })
                .catch((err) => {
                    socket.emit('error', 'Die displays konnten nicht ausgelesen werden. Fehler: ' + err);
                })
        } catch (err) {
            console.log(err);
        }
    }
}


class Singleton {
    constructor() {
        if (!Singleton.instance) {
            Singleton.instance = new socketIoAPI();
        }
    }

    getInstance() {
        return Singleton.instance;
    }

    setSocket(givenSocket) {
        socket = givenSocket;
    }
}

module.exports = Singleton;