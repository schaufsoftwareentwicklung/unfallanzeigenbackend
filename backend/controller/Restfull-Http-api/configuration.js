const ConfigurationService = require('../../services/configurationService');

var configurationService = new ConfigurationService().getInstance();

exports.addDisplay = (req, res, next) => {
    try {
        configurationService.addDisplay(req.body)
            .then((createddisplay) => {
                res.status(201).json({
                    message: 'Die Anzeige wurde erfolgreich hinzugefügt',
                    _id: createddisplay._id
                });
            })
            .catch((err) => {
                res.status(202).json({
                    message: 'Die Anzeigenkonfiguration konnte nicht hinzugefügt werden. Error: ' + err,
                    _id: null
                })
            })
    } catch (err) {
        console.log(err);
        res.status(202).json({
            message: 'Die Anzeigenkonfiguration konnte nicht hinzugefügt werden. Error: ' + err,
            _id: null
        })
    }
}


exports.getDisplays = (req, res, next) => {
    try {
        configurationService.getDisplays()
            .then((displays) => {
                res.status(200).json({
                    message: 'Display wurden erfolgreich geladen',
                    displays: displays
                });
            })
            .catch((err) => {
                res.status(202).json({
                    message: 'Die Anzeigenkonfiguration konnte nicht geladen werden. Error: ' + err,
                    _id: null
                })
            })
    } catch (err) {
        console.log(err);
        res.status(202).json({
            message: 'Die Anzeigenkonfiguration konnte nicht geladen werden. Error: ' + err,
            _id: null
        })
    }
}

exports.deleteDisplay = (req, res, next) => {
    try {
        configurationService.deleteDisplay(req.params.id)
            .then(() => {
                res.status(200).json({
                    message: 'Display wurden erfolgreich gelöscht',
                });
            })
            .catch((err) => {
                res.status(202).json({
                    message: 'Die Anzeigenkonfiguration konnte nicht gelöscht werden. Error: ' + err,
                    _id: null
                })
            })
    } catch (err) {
        console.log(err);
        res.status(202).json({
            message: 'Die Anzeigenkonfiguration konnte nicht gelöscht werden. Error: ' + err,
            _id: null
        })
    }
}

exports.patchDisplay = (req, res, next) => {
    try {
        configurationService.patchDisplay(req.params.id, req.body)
            .then(() => {
                res.status(200).json({
                    message: 'Die Anzeigenkonfiguration wurde erfolgreich geupdatet.',
                });
            })
            .catch((err) => {
                res.status(202).json({
                    message: 'Die Anzeigenkonfiguration konnte nicht geupdatet werden. Error: ' + err,
                })
            })
    } catch (err) {
        console.log(err);
        res.status(202).json({
            message: 'Die Anzeigenkonfiguration konnte nicht geupdatet werden. Error: ' + err,
        })
    }
}