const WorkingService = require('../../services/workingService');

var workingService = new WorkingService().getInstance();


exports.getAccidentData = (req, res, next) => {
    try {
        workingService.getAccidentData(req.params.id, req.params.interfaceIndex)
            .then((display) => {
                res.status(201).json({
                    message: 'Die Daten der Anzeige wurde erfolgreich ausgelesen.',
                    status: 201,
                    display: display
                });
            })
            .catch((err) => {
                let error = "";
                if (err){
                    error = 'Die Daten der Anzeige konnte nicht ausgelesen werden. Error: ' + err.toString();
                }
                else {
                    error = 'Die Daten der Anzeige konnte nicht ausgelesen werden.';
                }
                    res.status(202).json({
                        message: error,
                        status: 202
                    });
            })
    } catch (err) {
        console.log(err);
        res.status(202).json({
            message: 'Die Daten der Anzeige konnte nicht ausgelesen werden. Error: ' + err.toString(),
            status: 202
        });
    }
}

exports.sendAccidentData = (req, res, next) => {
    try {
      workingService.sendAccidentData(req.body)
            .then((result) => {
                res.status(201).json({
                    message: result.message,
                    status: 201,
                    display: result.display
                });
            })
            .catch((err) => {
                res.status(202).json({
                    message: 'Die Anzeige wurde nicht geupdated. Error: ' + err.toString(),
                    status: 202
                });
            })
    } catch (err) {
        console.log(err);
        res.status(202).json({
            message: 'Die Anzeige wurde nicht geupdated. Error: ' + err.toString(),
            status: 202
        });
    }
}

exports.sendDateTimeData = (req, res, next) => {
    try {
        workingService.sendDateTimeData(req.body)
            .then((result) => {
                res.status(201).json({
                    message: result.message,
                    status: 201,
                    display: result.display
                });
            })
            .catch((err) => {
                res.status(202).json({
                    message: 'Die Anzeige wurde nicht geupdated. Error: ' + err.toString(),
                    status: 202
                });
            })
    } catch (err) {
        console.log(err);
        res.status(202).json({
            message: 'Die Anzeige wurde nicht geupdated. Error: ' + err.toString(),
            status: 202
        });
    }
}

exports.reportAccident = (req, res, next) => {
    try {
        workingService.reportAccident(req.body)
            .then((result) => {
                res.status(201).json({
                    message: result.message,
                    status: 201,
                    display: result.display
                });
            })
            .catch((err) => {
                res.status(202).json({
                    message: 'Die Anzeige wurde nicht geupdated. Error: ' + err.toString(),
                    status: 202
                });
            })
    } catch (err) {
        console.log(err);
        res.status(202).json({
            message: 'Die Anzeige wurde nicht geupdated. Error: ' + err.toString(),
            status: 202
        });
    }
}