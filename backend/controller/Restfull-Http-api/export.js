const exportService = require('../../services/exportService');


exports.getWebsite = (req, res, next) => {
    try {
        exportService.getWebsite(req.body)
            .then((webComponent) => {
                res.send(getHTMLString(webComponent));
            })
            .catch((err) => {
                res.send(getHTMLString('<div>Leider konnten die Displays nicht geladen werden. Error: ' + err + '</div>'));
            })

    } catch (err) {
        console.log(err);
        res.send(getHTMLString('<div>Leider konnten die Displays nicht geladen werden. Error: ' + err + '</div>'));
    }
}

function getHTMLString(toHtml) {
    return toHtml.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
}


exports.getCSV = (req, res, next) => {
    res.status(200).json({
        message: 'not implemented yet',
        displays: null
    });
}