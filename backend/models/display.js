const mongoose = require('mongoose');
const DisplayInterfaceType = Object.freeze({"Network" : "Network", "RS232" : "RS232" });


const displaySchema = mongoose.Schema({
    name: {type: String},
    useAccidentsLastYear: {type: Boolean},
    useAccidentsThisYear: {type: Boolean},
    useDaysWithoutAccidents: {type: Boolean},
    useDaysWithoutAccidentsHighscore: {type: Boolean},
    useGtxText: {type: Boolean},
    gtxRows: {type: Number},
    gtxTextLength: {type: Number},
    displayInterface: {type: String},
    interfaces: [String],
    manualModus: {type: Boolean, default: true},
    time: {type: String},
    date: {type: String},
    displayContent: {
        accidentsLastYear: Number, default: 0,
        accidentsThisYear: Number, default: 0,
        daysWithoutAccidents: Number, default: 0,
        daysWithoutAccidentsHighscore: Number, default: 0,

        gtxTextFirstRow: String, default: "",
        gtxTextSecoundRow: String, default: ""
    }
});

 module.exports =    mongoose.model('Display', displaySchema);