const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const exportController = require('./controller/Restfull-Http-api/export');
const configurationController = require('./controller/Restfull-Http-api/configuration');
const workingController = require('./controller/Restfull-Http-api/working');

const app = express();


mongoose.connect("mongodb://127.0.0.1:27017", { useNewUrlParser: true, useUnifiedTopology: true, poolSize: 10, useFindAndModify: false })
    .then(() => {
        console.log('Connected to Database')
    })
    .catch(() => {
        console.log('Connection failed');
    });


// app.use(express.static(path.join(__dirname + '/publictestchat')));

app.use(bodyParser.json());

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, PATCH, DELETE, OPTIONS"
    );
    next();
});

//configuration
app.post("/api/display/add", configurationController.addDisplay);
app.delete("/api/display/delete/:id", configurationController.deleteDisplay);
app.get("/api/display/get-all", configurationController.getDisplays);
app.patch("/api/display/update/:id", configurationController.patchDisplay);

//working
app.get("/api/display/get-accident-data/:id/:interfaceIndex", workingController.getAccidentData);
app.post("/api/display/report-accident/", workingController.reportAccident);
app.post("/api/display/send-accident-data/", workingController.sendAccidentData);
app.post("/api/display/send-date-time-data/", workingController.sendDateTimeData);

//export
app.get("/api/export/website/get-all", exportController.getWebsite);
app.get("/api/export/csv/get-all", exportController.getCSV);

//errorhandling
app.use((error, req, res, next) => {
    console.log(error);
    const status = error.statusCode || 500;//statuscode defined by me if not use 500
    const message = error.message;//builtin
    const data = error.data;
    res.status(status).json({ message: message, data: data });
});

//Error logic

// if(!err.statusCode) { //if no statuscode exist
//   err.statusCode = 500;
// }
// next(err);//go to error










module.exports = app;