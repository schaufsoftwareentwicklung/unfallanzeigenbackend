const ConfigurationService = require('./services/configurationService');
const WorkingService = require('./services/workingService');
const MonitorService = require('./services/monitorService');
const SocketIoAPI = require('./controller/socket-io-api/monitor');


var io;
var socketIoAPI;

class Socket {
  constructor(socket) {
    io = socket;

    this.setSocketsInServices();
    this.setSocketsInAPIs();

    io.on('connection', (socket, next) => {
      console.log('New Client Connection.....' + socket.id);

      socket.on('disconnect', () => {
        console.log('user disconnected ' + socket.id);
      });

      socket.on('getDisplays', () => {
        socketIoAPI.getDisplays();
      });
    });


  }

  setSocketsInServices() {
    new ConfigurationService().setSocket(io);
    new WorkingService().setSocket(io);
    new MonitorService().setSocket(io);
  }

  setSocketsInAPIs() {
    let socketCreator = new SocketIoAPI();
    socketCreator.setSocket(io);
    socketIoAPI = socketCreator.getInstance();
  }
}

module.exports = Socket;