const Display = require('../models/display');
const accidentDataReader = require('../protocolls/accidentDataReader');
const accidentDataSender = require('../protocolls/accidentDataSender');

var socket;

class workingService {
    constructor() {

    }

    getAccidentData(id, interfaceIndex) {
        return new Promise(async (resolve, reject) => {
            try {
                let display = await Display.findById(id);

                accidentDataReader.readAccidentData(display, interfaceIndex)
                    .then(readedDisplay => {
                            Display.findByIdAndUpdate({ _id: display._id }, { $set: readedDisplay })
                                .catch(err => {
                                    console.log('\'getAccidentData\' update display error: ' + err.toString());
                                })
                        resolve(readedDisplay);
                    })
                    .catch(err => {
                        reject(err);
                    });
            } catch (err) {
                reject(err);
            }
        });
    }
    sendAccidentData(display) {
        return new Promise((resolve, reject) => {
            try {
                accidentDataSender.sendAccidentData(display)
                    .then(response => {
                        Display.findByIdAndUpdate({ _id: response.display._id }, { $set: response.display })
                            .catch(err => {
                                console.log('\'sendAccidentData\' update display error: ' + err.toString());
                            })

                        if (socket) {
                            socket.emit("displayChanged", response.display);
                        }

                        resolve(response);
                    })
                    .catch(err => {
                        reject(err);
                    });
            } catch (err) {
                reject(err);
            }
        });
    }
    sendDateTimeData(display) {
        return new Promise((resolve, reject) => {
            try {
                accidentDataSender.sendDateTimeData(display)
                    .then(response => {
                        Display.findByIdAndUpdate({ _id: response.display._id }, { $set: response.display })
                            .catch(err => {
                                console.log('\'sendDateTimeData\' update display error: ' + err.toString());
                            })

                        resolve(response);
                    })
                    .catch(err => {
                        reject(err);
                    });
            } catch (err) {
                reject(err);
            }
        });
    }
    reportAccident(display) {
        return new Promise(async (resolve, reject) => {
            try {
                display = await Display.findById(display._id);

                await accidentDataReader.readAccidentData(display, 0)
                    .then(readedDisplay => {
                        display = readedDisplay;
                    })
                    .catch(err => {
                        reject(err.toString());
                    });

                await accidentDataSender.reportAccident(display)
                    .then(response => {
                        Display.findByIdAndUpdate({ _id: response.display._id }, { $set: response.display })
                            .catch(err => {
                                console.log('\'reportAccident\' update display error: ' + err.toString());
                            })

                        if (socket) {
                            socket.emit("displayChanged", response.display);
                        }

                        resolve(response);
                    })
                    .catch(err => {
                        reject(err);
                    });
            } catch (err) {
                reject(err);
            }
        });
    }
}


class Singleton {
    constructor() {
        if (!Singleton.instance) {
            Singleton.instance = new workingService();
        }
    }

    getInstance() {
        return Singleton.instance;
    }

    setSocket(givenSocket) {
        socket = givenSocket;
    }
}

module.exports = Singleton;