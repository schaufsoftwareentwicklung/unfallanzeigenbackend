const Display = require('../models/display');
const Schedule = require('node-schedule');
const accidentDataReader = require('../protocolls/accidentDataReader');

var socket;
var job;

class monitorService {
    constructor() {
       job = Schedule.scheduleJob('0 05 0 * * *', () => {
            this.updateDisplays();
       });
    }

    getDisplays(){
        return new Promise(async (resolve, reject) => {
            try{
                Display.find().then(displays => {
                    resolve(displays);
                })
                .catch(err => {
                    reject(err);
                })
            } catch(err){
                reject(err);
            }
        });
    }

    updateDisplays() {
        try{
            Display.find().then(async displays => {
                for (let i = 0; i < displays.length; i++) {
                    console.log('---------  ' + i + '  ---------')
                    try{
                        await  accidentDataReader.readAccidentData(displays[i], 0)
                        .then(async (newDisplay) => {
                            if (newDisplay != displays[i]){
                                displays[i] = newDisplay;
                                await Display.findByIdAndUpdate({_id: displays[i]._id}, { $set: newDisplay })
                                .then()
                                .catch(err => {
                                    console.log('\'updateDisplays\' update display error: ' + err.toString());
                                })
                            }
                        })
                        .catch(err => {
                            console.log(err.toString());
                        })
                    } catch (err){
                        console.log(err.toString());        
                    }
                }
                    
                socket.emit('sendDisplays', displays);
            })
            .catch(err => {
                console.log(err.toString())
            })

            
        } catch(err){
            console.log(err.toString());
        }
    }
}


class Singleton {
    constructor() {
        if (!Singleton.instance){
            Singleton.instance = new monitorService();
        }
    }

    getInstance() {
        return Singleton.instance;
    }

    setSocket(givenSocket) {
        socket = givenSocket;
    }
}

module.exports = Singleton;