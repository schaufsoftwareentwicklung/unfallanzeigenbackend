var mongoose = require('mongoose');
const Display = require('../models/display');

var socket;

class configurationService {
    constructor() {

    }

    addDisplay(displayJson) {
        return new Promise((resolve, reject) => {
            try {
                let display = new Display(displayJson);
                display._id = mongoose.Types.ObjectId();
                display.save().then(createddisplay => {
                    resolve(createddisplay);
                });
                if (socket) {
                    socket.emit("displayAdded", display);
                }
            } catch (err) {
                reject(err);
            }
        });
    }

    getDisplays() {
        return new Promise((resolve, reject) => {
            try {
                Display.find().then(displays => {
                    resolve(displays);
                })
                    .catch(err => {
                        reject(err);
                    })
            } catch (err) {
                reject(err);
            }
        });
    }

    deleteDisplay(id) {
        return new Promise((resolve, reject) => {
            try {
                Display.deleteOne({ _id: id })
                    .then(() => {

                        if (socket) {
                            socket.emit("displayDeleted", id);
                        }
                        resolve();
                    })
                    .catch(err => {
                        reject(err);
                    })
            } catch (err) {
                reject(err);
            }
        });
    }

    patchDisplay(id, display) {
        return new Promise((resolve, reject) => {
            try {
                Display.findByIdAndUpdate({ _id: id }, { $set: display })
                    .then(() => {
                        if (socket) {
                            socket.emit("displayChanged", display);
                        }
                        resolve();
                    })
                    .catch((err) => {
                        reject(err);
                    })
            } catch (err) {
                reject(err);
            }
        });
    }
}


class Singleton {
    constructor() {
        if (!Singleton.instance) {
            Singleton.instance = new configurationService();
        }
    }

    getInstance() {
        return Singleton.instance;
    }

    setSocket(givenSocket) {
        socket = givenSocket;
    }
}

module.exports = Singleton;