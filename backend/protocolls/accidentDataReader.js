const transreciver = require("./transreciver");

module.exports = {
    readAccidentData: function (display, selectedInterface) {
        return new Promise((resolve, reject) => {
            try {
                if (display === undefined || display === null) {
                    throw "Das Display wurde nicht gefunden/gesendet";
                }

                transreciver.getDisplayData(display, selectedInterface)
                    .then(displayData => {
                        display = getDisplayFromProtocol(displayData, display);
                        
                        resolve(display);
                    })
                    .catch(error => {
                        reject(error);
                    })
            } catch (error) {
                reject(error);
            }
        });
    }
}

function getDisplayFromProtocol(protocol, display) {
    protocol = protocol.split('0d')
    let hexArray = [];
    for (var i = 0; i < protocol.length - 1; i++) {
        protocol[i] = protocol[i] + '0d';
        hexArray[i] = Buffer.from(protocol[i], 'hex');
    }

    hexArray.forEach(hex => {
        if (hex[0] === 27 && hex[1] === 41) {
            switch (hex[2]) {
                case 32:
                    display.displayContent.daysWithoutAccidents = hex.slice(3, hex.length - 1).toString('utf8');
                    break;
                case 33:
                    display.displayContent.accidentsLastYear = hex.slice(3, hex.length - 1).toString('utf8');
                    break;
                case 34:
                    display.displayContent.accidentsThisYear = hex.slice(3, hex.length - 1).toString('utf8');
                    break;
                case 35:
                    let time = hex.slice(3, hex.length - 1).toString('utf8');
                    display.time = time.slice(0, 2) + ':' + time.slice(2);
                    break;
                case 36:
                    let date = hex.slice(3, hex.length - 1).toString('utf8');
                    display.date = date.slice(0, 2) + '.' + date.slice(2, 4) + "." + date.slice(4, 6);
                    break;
                case 37:
                    display.displayContent.daysWithoutAccidentsHighscore = hex.slice(3, hex.length - 1).toString('utf8');
                    break;
                default:
                    break;
            }
        }
    })

    return display;
}



