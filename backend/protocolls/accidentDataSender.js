const transreciver = require('./transreciver');
const hexConverter = require('./hexConverter');
const moment = require('moment');

module.exports = {
    sendAccidentData: function (display) {
        return new Promise((resolve, reject) => {
            try {
                if (display === undefined || display === null) {
                    throw ("Das Display wurde nicht gefunden/gesendet");
                }


                let accidentData = "";
                let textData = "";
                let protocolData;


                if (display.useAccidentsLastYear || display.useAccidentsThisYear ||
                    display.useDaysWithoutAccidents || display.useDaysWithoutAccidentsHighscore) {
                    accidentData = getAccidentProtocol(display, true);
                }

                if (display.useGtxText) {
                    textData = getGTXProtocol(display);
                }

                if (accidentData !== "" && textData !== "") {
                    protocolData = Buffer.concat([accidentData, textData]);
                } else if (textData === "") {
                    protocolData = accidentData;
                } else {
                    protocolData = textData;
                }

                sendToDisplay(display, protocolData)
                    .then((result) => {
                        resolve({ display: display, message: result });
                    })
                    .catch((error) => {
                        reject(error);
                    })
            } catch (error) {
                reject(error);
            }
        });
    },
    sendDateTimeData: function (display) {
        return new Promise((resolve, reject) => {
            try {
                if (display === undefined || display === null) {
                    throw ("Das Display wurde nicht gefunden/gesendet");
                }

                let protocollData = getDateTimeProtocol(display);

                if (protocollData.error !== "") {
                    throw (protocollData.error);
                }

                sendToDisplay(display, protocollData.protocol)
                    .then((result) => {
                        resolve({ display: display, message: result });
                    })
                    .catch((error) => {
                        reject(error);
                    })
            } catch (error) {
                reject(error);
            }
        });
    },
    reportAccident: function (display) {
        return new Promise((resolve, reject) => {
            try {
                display.displayContent.accidentsThisYear += 1;
                display.displayContent.daysWithoutAccidents = 0;

                let protocolData = getAccidentProtocol(display, false);

                sendToDisplay(display, protocolData)
                    .then((result) => {
                        resolve({ display: display, message: result });
                    })
                    .catch((error) => {
                        reject(error);
                    })

            } catch (error) {
                reject(error);
            }
        });
    }
}

async function sendToDisplay(display, protocollData) {
    return new Promise(async (resolve, reject) => {
        try {
            let errors = "";
            let successes = "";

            if (display.displayInterface === "Network") {
                for (const interface of display.interfaces) {
                    await transreciver.sendAccidentDataToDisplayViaTCPIP(interface, protocollData)
                        .then(() => {
                            successes += interface.toString() + '\n';
                        })
                        .catch(err => {
                            errors += err + '\n';
                        })
                }
            }
            else {
                for (const interface of display.interfaces) {
                    await transreciver.sendAccidentDataToDisplayViaPort(interface, protocollData)
                        .then(() => {
                            successes += interface.toString() + '\n';
                        })
                        .catch(err => {
                            console.log(err);
                            errors += err + '\n';
                        })
                }
            }

            let result = "";
            if (errors != "" && successes != "") {
                result = 'Die Anzeigen mit folgenden Schnittstellen wurden erfolgreich geupdatet:\n' + successes + 'Es gab folgendende fehler:\n' + errors;
            } else if (successes != "") {
                result = 'Die Anzeigen mit folgenden Schnittstellen wurden erfolgreich geupdatet:\n' + successes
            } else {
                'Es gab folgendende fehler:\n' + errors;
            }

            resolve(result);
        } catch (err) {
            reject(err);
        }
    });
}

function getAccidentProtocol(display, getHighscore) {
    let protocol = "";


    if (display.useAccidentsLastYear || display.useAccidentsThisYear) {

        let last = display.displayContent.accidentsLastYear.toString();
        let thiss = display.displayContent.accidentsThisYear.toString();

        thiss = setStringLength(thiss, 2);
        last = setStringLength(last, 2);

        let hexLast = hexConverter.getASCIIHexValueFromIntAsString(last);
        let hexThis = hexConverter.getASCIIHexValueFromIntAsString(thiss);


        protocol += "1B2120" + hexLast + hexThis + "0D";
    }

    if (display.useDaysWithoutAccidents) {
        let accidentsWithout = display.displayContent.daysWithoutAccidents.toString();
        accidentsWithout = setStringLength(accidentsWithout, 4);

        let hexAccidentsWithout = hexConverter.getASCIIHexValueFromIntAsString(accidentsWithout);

        protocol += "1B2122" + hexAccidentsWithout + "0D";
    }

    if (display.useDaysWithoutAccidentsHighscore && getHighscore) {
        let accidentsWithoutHighscore = display.displayContent.daysWithoutAccidentsHighscore.toString();
        accidentsWithoutHighscore = setStringLength(accidentsWithoutHighscore, 4);
        let hexAccidentsWithoutHighscore = hexConverter.getASCIIHexValueFromIntAsString(accidentsWithoutHighscore);

        protocol += "1B2123" + hexAccidentsWithoutHighscore + "0D";
    }

    if (protocol !== "") {
        return Buffer.from(protocol, 'hex');
    } else {
        return undefined;
    }
}

function getGTXProtocol(display) {
    //header
    let protocol = "00";

    let numberOfLines = "0" + display.gtxRows;

    //DEBUG!!!
    numberOfLines = "03";
    //DEBUG!!!

    protocol += numberOfLines;
    let signadress = "01";
    protocol += signadress;
    protocol += "03";

    //serialStatus
    protocol += "C0";

    //pageNumber
    protocol += "303031";

    //tempo of Page
    // 11101001
    /// bite 7 - 4  = immer an
    /// bite 3 - 0 = tempo aktuell 9
    protocol += "E9";
    // protocol += "D6";

    //GTXFunction CF = Slide
    protocol += "CF";
    // protocol += "CF";

    //siteState = keine zeilen zusammenziehen
    // Hintergrundfarbe aus = A0
    protocol += "A0";

    //Steuerzeichen optional (für farben, blinken oder Breitschrift)
    //mehrfarbig
    // protocol += "1C4D";
    //standart
    protocol += "1C44";

    //Message

    //DEBUG!!
    //protocol += getTextInHex("DEBUG:  O-('-'Q)");
    //DEBUG!!

    if (display.gtxRows === 1) {
        protocol += getTextInHex(display.displayContent.gtxTextFirstRow, display.gtxTextLength);
    } else {
        protocol += getTextInHex(getRowInRightLength(display.displayContent.gtxTextFirstRow, display.gtxTextLength));
        if (display.gtxRows === 2) {
            protocol += getTextInHex(display.displayContent.gtxTextSecoundRow, display.gtxTextLength);
        }
    }


    //eot 
    protocol += "04";

    //checksum
    let hexbuffer = Buffer.from(protocol, 'hex');

    let checksum = hexbuffer[0];
    for (var i = 1; i < hexbuffer.length; i++) {
        checksum ^= hexbuffer[i];
    }
    if (checksum.toString().length === 1) {
        checksum = "0" + checksum.toString();
    }

    let checksumBuffer = Buffer.from(checksum.toString(16), 'hex');

    return Buffer.concat([hexbuffer, checksumBuffer]);
}

function setStringLength(stringToExpand, size) {
    for (var i = stringToExpand.length; i < size; i++) {
        stringToExpand = "0" + stringToExpand;
    }
    return stringToExpand;
}

function getRowInRightLength(text, length) {
    let stringToExpand = text;
    for (var i = text.length; i < length; i++) {
        stringToExpand += " ";
    }

    return stringToExpand;
}

function getTextInHex(text) {
    hexString = "";
    for (var i = 0; i < text.length; i++) {
        if (text[i] === "ü" || text[i] === "Ü" ||
            text[i] === "ö" || text[i] === "Ö" ||
            text[i] === "ä" || text[i] === "Ä") {
            switch (text[i]) {
                case 'ü':
                    hexString += "9A";
                    break;
                case 'Ü':
                    hexString += "9A";
                    break;
                case 'ä':
                    hexString += "8E";
                    break;
                case 'Ä':
                    hexString += "8E";
                    break;
                case 'Ö':
                    hexString += "99";
                    break;
                case 'ö':
                    hexString += "99";
                    break;
            }
        } else {
            hexString += text[i].charCodeAt(0).toString(16);
        }
    }

    return hexString;
}

function getDateTimeProtocol(display) {
    let protocol = "";
    let error = "";
    const timeRegex = new RegExp('^([0-2][0-3]:[0-5][0-9]|[0-1][0-9]:[0-5][0-9])$');


    if (timeRegex.test(display.time)) {
        let time = display.time.toString();
        time = time.replace(':', '');
        let hexTime = hexConverter.getASCIIHexValueFromIntAsString(time);

        protocol += "1B2220" + hexTime + "0D";
    } else {
        error += "Die Zeit war falsch formatiert.";
    }
    if (moment(display.date, "DD.MM.YY").isValid()) {

        let date = display.date.toString();
        date = date.replace('.', '');
        date = date.replace('.', '');
        let hexDate = hexConverter.getASCIIHexValueFromIntAsString(date);

        protocol += "1B2320" + hexDate.substr(0, 8) + "0D";
        protocol += "1B24200000" + hexDate.substr(8, 4) + "0D";
    } else {
        error += "Das Datum war falsch formatiert.";
    }


    return { protocol: Buffer.from(protocol, 'hex'), error: error };
}