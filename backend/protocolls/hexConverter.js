module.exports = {
    getASCIIHexValueFromIntAsString(stringToConvert) {
        let hex = "";
    
        for (var i = 0; i < stringToConvert.length; i++) {
            switch (stringToConvert[i]) {
                case "0":
                    hex += "30";
                    break;
                case "1":
                    hex += "31";
                    break;
                case "2":
                    hex += "32";
                    break;
                case "3":
                    hex += "33";
                    break;
                case "4":
                    hex += "34";
                    break;
                case "5":
                    hex += "35";
                    break;
                case "6":
                    hex += "36";
                    break;
                case "7":
                    hex += "37";
                    break;
                case "8":
                    hex += "38";
                    break;
                case "9":
                    hex += "39";
                    break;
                default:
                    hex += "30";
                    break;
            }
        }
    
        return hex;
    }
}