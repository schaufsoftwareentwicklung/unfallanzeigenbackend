var net = require('net');
const SerialPort = require('serialport');
const InterByteTimeout = require('@serialport/parser-inter-byte-timeout')

module.exports = {
    sendAccidentDataToDisplayViaTCPIP(interface, protocol) {
        return new Promise((resolve, reject) => {
            var client = new net.Socket();
            try {
                var timer;
                var timeout = 2500;
                var ipPort = interface.split(':');

                client.setEncoding('hex');

                client.connect(ipPort[1], ipPort[0], function () {
                    clearTimeout(timer);
                    client.write(protocol);
                    client.destroy();
                    setTimeout(() => {
                        resolve("success");
                    }, 350)
                });

                client.on('error', function (err) {
                    clearTimeout(timer);
                    client.destroy();
                    setTimeout(() => {
                        reject(err);
                    }, 350)
                })

                timer = setTimeout(function () {
                    console.log("[ERROR] Attempt at connection exceeded timeout value");
                    client.destroy();
                    reject("Connection for interface: " + interface + " got a timout (2500 ms)");
                }, timeout);
            } catch (error) {
                if (client !== undefined) {
                    client.destroy();
                }
                setTimeout(() => {
                    reject(error);
                }, 350)
            }
        })
    }
    , async sendAccidentDataToDisplayViaPort(interface, protocol) {
        return new Promise((resolve, reject) => {

            let port = new SerialPort(interface, {
                baudRate: 9600,
                dataBits: 8,
                parity: 'none',
                stopBits: 1,
                autoOpen: false
            });

            try {

                port.open((error) => {
                    if (error) {
                        if (port.isOpen) {
                            port.close((error) => {
                                console.log(error);
                            });
                        }
                        setTimeout(() => {
                            reject(error);
                        }, 350)
                    }
                });

                port.write(protocol, function (error) {
                    if (error) {
                        if (port.isOpen) {
                            port.close((error) => {
                                console.log(error);
                            });
                        }
                        setTimeout(() => {
                            reject(error);
                        }, 350)
                    }
                })

                let received = "";
                port.on('data', function (data) {
                    received = received + data.toString('hex');;

                    if ((received.match(/0d/g) || []).length == 6) {
                        if (port.isOpen) {
                            port.close((error) => {
                                console.log(error);
                            });
                        }
                        setTimeout(() => {
                            resolve(received);
                        }, 350)
                    }
                })

                port.on('error', function (error) {
                    if (port.isOpen) {
                        port.close((error) => {
                            console.log(error);
                        });
                    }

                    setTimeout(() => {
                        reject(error);
                    }, 350)
                })


                port.on('close', (error) => {
                    if (port.isOpen) {
                        port.close((error) => {
                            console.log(error);
                        });
                    }
                    setTimeout(() => {
                        reject(error);
                    }, 350)
                })

            } catch (error) {
                if (port !== undefined && port !== null) {
                    if (port.isOpen) {
                        port.close((error) => {
                            console.log(error);
                        });
                    }
                }
                setTimeout(() => {
                    reject(error);
                }, 350)
            }
        })
    },
    getDisplayData(display, selectedInterface) {
        return new Promise((resolve, reject) => {
            const rawhex = "1B25200D";
            const datagram = Buffer.from(rawhex, 'hex');

            if (display.displayInterface == 'Network') {
                var client = new net.Socket();
                try {
                    var timer;
                    var timeout = 2500;
                    var ipPort = display.interfaces[selectedInterface].split(':');

                    client.setEncoding('hex');
                    client.connect(ipPort[1], ipPort[0], function () {
                        clearTimeout(timer);
                        client.write(datagram);
                    });


                    let received = "";
                    client.on('data', function (data) {
                        clearTimeout(timer);
                        received = received + data;


                        if ((received.match(/0d/g) || []).length == 6) {
                            client.destroy();
                            setTimeout(() => {
                                resolve(received);
                            }, 350)
                        }
                    });

                    client.on('error', function (err) {
                        clearTimeout(timer);
                        console.log(err);
                        client.destroy();
                        setTimeout(() => {
                            reject(err);
                        }, 350)
                    })

                    timer = setTimeout(function () {
                        console.log("[ERROR] Attempt at connection exceeded timeout value");
                        client.destroy();
                        reject("Connection for interface: " + display.interfaces[selectedInterface] + " got a timout (2500 ms)");
                    }, timeout);
                } catch (error) {
                    if (client !== undefined) {
                        client.destroy();
                    }
                    setTimeout(() => {
                        reject(error);
                    }, 350)
                }
            }
            else {

                var port = new SerialPort(display.interfaces[selectedInterface], {
                    baudRate: 9600,
                    dataBits: 8,
                    parity: 'none',
                    stopBits: 1,
                    autoOpen: false
                });

                
                try {

                    const parser = port.pipe(new InterByteTimeout({interval: 2500}))


                    // SerialPort.list().then(ports => {
                    //     ports.forEach(function(port) {
                    //         console.log(port.path)
                    //     })
                    // })


                    parser.on('data', () => {
                        if (port.isOpen){
                            port.close((error) => {
                                console.log(error);
                            });
                        }
                        reject(display.interfaces[selectedInterface].toString());
                    });

                    port.open((error) => {

                        if (error) {
                            if (port.isOpen) {
                                port.close((error) => {
                                    console.log(error);
                                });
                            }
                            setTimeout(() => {
                                reject(error);
                            }, 350)
                        }
                    });

                    port.write(datagram, function (error) {
                        if (error) {
                            if (port.isOpen) {
                                port.close((error) => {
                                    console.log(error);
                                });
                            }
                            setTimeout(() => {
                                reject(error);
                            }, 350)
                        }
                    })

                    let received = "";
                    port.on('data', function (data) {
                        received = received + data.toString('hex');;

                        if ((received.match(/0d/g) || []).length == 6) {
                            if (port.isOpen) {
                                port.close((error) => {
                                    console.log(error);
                                });
                            }
                            setTimeout(() => {
                                resolve(received);
                            }, 350)
                        }
                    })

                    port.on('error', function (error) {
                        if (port.isOpen) {
                            port.close((error) => {
                                console.log(error);
                            });
                        }

                        setTimeout(() => {
                            reject(error);
                        }, 350)
                    })


                    port.on('close', (error) => {
                        if (port.isOpen) {
                            port.close((error) => {
                                console.log(error);
                            });
                        }
                        setTimeout(() => {
                            reject(error);
                        }, 350)
                    })

                } catch (error) {
                    if (port !== undefined && port !== null) {
                        if (port.isOpen) {
                            port.close((error) => {
                                console.log(error);
                            });
                        }
                    }
                    setTimeout(() => {
                        reject(error);
                    }, 350)
                }
            }
        });
    }
}
